'use strict';

const Lob = require('./json/lob.json');
const LobEnum = require('./enum/lob-enum');
const States = require('./json/states.json');
const Statuses = require('./json/statuses.json');
const Attachments = require('./json/attachments.json');
const AttachmentEnum = require('./enum/attachment-enum');
const Cargo = require('./cargo/cargo');

// let LobEnum = ['CARGO' = 3, 'PLEASURECRAFT' = 1];
// console.log(Cargo);

// console.log(Lob,enumFn(Lob));
module.exports = {
    Lob,LobEnum,
    States,
    Statuses,
    Attachments,
    AttachmentEnum,
    Cargo
}