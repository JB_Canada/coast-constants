'use strict';

const CargoStates = require('./json/cargo-states.json');
const CargoQuoteStatuses = require('./json/cargo-quote-statuses.json');
const CargoPolicyStatuses = require('./json/cargo-policy-statuses.json');
const CargoForkStatuses = require('./json/cargo-fork-statuses.json');

module.exports = {
    CargoStates,
    CargoQuoteStatuses,
    CargoPolicyStatuses,
    CargoForkStatuses    
}