'use strict';

const attachmentEnum = [];
attachmentEnum.QUOTE = 1;
attachmentEnum.APPLICATION = 2;
attachmentEnum.NEW_POLICY = 3;
attachmentEnum.POLICY_RENEWAL = 4;

attachmentEnum.ATTACH_CERT = 101;
attachmentEnum.ENDORSEMENT = 102;

attachmentEnum.REVISED_DEC_POLICY = 200;
attachmentEnum.REVISED_DEC_CERT = 201;

module.exports = attachmentEnum